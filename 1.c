#include<stdio.h>
//Program to find the profit
int income(int);
int expend(int);
int attendance(int);
int profit(int);

void main()
{
    int price;
    printf("Enter ticket price\n");
    scanf("%d",&price);

    printf("Profit =%d\n",income(price));
    printf("Profit =%d\n",expend(price));
    printf("Profit =%d\n",profit(price));
}


//functions

//Function to find attendance
int attendance(int tprice)
{
    return 120+(15-tprice)*20/5;
}
//Function to find income
int income(int tprice)
{
    return tprice*attendance(tprice);
}

//Function to find expend
int expend(int tprice)
{
    return 500+(3*attendance(tprice));
}
//Function to find profit
int profit(int tprice)
{
    return income(tprice)-expend(tprice);
}
